<?php
include("conexao.php");
session_start();
$enviar = null;
$confirmar = null;
?>

<?php
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		
		if(!$_POST['message']){
            $sql_user = "INSERT INTO cadastro (
            nome,
            mail) 
            VALUES (
            '$_POST[name]',
            '$_POST[email]'
            )";
		}
		else{
			$sql_user = "INSERT INTO cadastro (
			nome,
			mail,
			msg
			) 
			VALUES (
			'$_POST[name]',
			'$_POST[email]',
			'$_POST[message]'
			)";
		}

		if($mysqli->query($sql_user) === TRUE)
			echo '<script>alert("Ação confirmada")</script>';   

		session_commit();
		$mysqli -> close();
	}
?>

<!DOCTYPE HTML>
<!--
	Strongly Typed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>SOVA IT Co.</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" >
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="no-sidebar is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<section id="header">
					<div class="container">

						<!-- Logo -->
							<h1 id="logo"><a href="index.html">SOVA IT Co.</a></h1>
							<p>Empresa de T.I. focada em desenvolver as melhores soluções de tecnologia para empresas.</p>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li>
										<a href="#" class="icon fas fa-bookmark"><span>Produtos e parcerias</span></a>
										<ul>
											<li><a href="https://hotm.art/SiteOficialDicionario">O Dicionário De T.I.</a></li>
											
										</ul>
									</li>
									<li><a class="icon brands fa-facebook-f" href="https://www.facebook.com/sovaitco"><span>Facebook</span></a></li>
									<li><a class="icon brands fa-instagram" href="https://www.instagram.com/sovaitco"><span>Instagram</span></a></li>
									<li><a class="icon brands fab fa-linkedin" href="https://www.linkedin.com/company/sovaitco"><span>LinkedIn</span></a></li>
									<li><a class="icon brands fab fa-linkedin" href="intro.php"><span>LinkedIn</span></a></li>
								</ul>
							</nav>

					</div>
				</section>

			<!-- Main -->
				<section id="main">
					<div class="container">
						<div id="content">

							<!-- Post -->
								<article class="box post">
									<header>
										<h2>Bem vindo! Está é a <strong>SOVA IT Co.</strong> <br />
											Aproveite o seu tempo da melhor maneira!</h2>
									</header>
									<span class="image featured"><img src="images/capa-fb-final.png" alt="" /></span>
									<h3>E sim, é basicamente isso</h3>
									<p>A tarefa é o de menos, o foco, a dedicação e os cuidados são todos para
										você. 
										<br>Quer receber o melhor conteúdo de tecnologia? <strong>Inscreva-se já!</strong></br>
										<div class="row">
											<div class="col-6 col-12-medium">
												<section>
													<form role="form" method="post" >
														<div class="row gtr-50">
															<div class="col-6 col-12-small">
																<input name="name" placeholder="Nome" type="text" required>
															</div>
															<div class="col-6 col-12-small">
																<input name="email" placeholder="Email" type="text" required>
															</div>
														</div>
														<br><input type="submit" class="form-button button icon solid fa-envelope" name="confirmar" id="confirmar" value="Quero me inscrever"><br />
													</form>
												</section>
											</div>
										</div>
									</p>
									<p>SOVA IT Co. é uma empresa de software que conta com capital humano 
										profissional especializado de T.I. A empresa está localizada em 
										Indaiatuba no interior de São Paulo.</p>
									<p>O cenário mostra que a TI tem um papel fundamental especialmente
										em meio à adversidade para desencadear processos de transformação
										rápida e que permitam ganhos de produtividade e agilidade. Hoje, a 
										infraestrutura de TI estpa composta por cinco elementos principais:
										 hardware, software, tecnologias de gestão de dados, tecnologias de 
										 rede e telecomunicações e serviços de tecnologias. Esses elementos 
										 precisam ser coordenados entre si.</p>
									<h3>Conheça O dicionário de T.I.</h3>
									<strong>O Dicionário De T.I.</strong> é uma <strong>ferramenta extremamente poderosa
									</strong> para te auxiliar no seu dia a dia. É um <strong>e-book</strong> completo 
									desenvolvido especialmente para:
									<p> 1- Procurar palavras de maneira rápida para se atualizar com informações úteis 
										que envolvem tecnologias de uso diário .
										<br>2- Auxiliar profissionais de empresas que buscam se adaptar a ambientes de 
										alta demanda de serviços técnicos.<br />
										3- Dar robustez para você e sua comunicação na hora de falar com o seu chefe, 
										sobre aqueles termos "chatos"...<br />
										4- Profissionais de T.I. recém formados ou que estão se atualizando após tempo 
										fora do mercado.<br />
										5- Ter um ótimo glossário de cabeceira na palma da sua mão para poder consultar 
										rápido.<br />
										6- Estar sempre ligado e saber que o importante é estar com todas as informações 
										sempre a mão.<br />
																				
										<br><strong>✱</strong> E-book de uso diário feito com muito carinho e que receberá constantes atualizações. <strong>✱</strong><br />

										<br><a href="https://hotm.art/SiteOficialDicionario" class="form-button button icon solid fa-book">Comprar E-book</a><br />
										</p>
									<h3>Acompanhe a gente mais perto</h3>
									<p>As redes sociais hoje são de grande uso para empresas se comunicarem,
										afim de alcançar você, para estar atento a todos os passos da tecnlogia.
									<br>Siga-nos nas redes sociais!</br></p>
									<p>Impulsionada pela demanda do cliente e possibilitada pela tecnologia moderna, 
										a personalização em massa é uma tendência crescente em manufatura.</p>

									<p>Estudo do Gartner prevê que os gastos com TI no Brasil chegarão a US$ 64 bilhões 
										em 2020, um aumento de 2,5% em relação aos dados que temos até o momento em 2019. 
										Já os investimentos em Tecnologia da Informação no mercado brasileiro, este ano, 
										deverão diminuir 4,6%.</p>
								</article>

						</div>
					</div>
				</section>

			<!-- Footer -->
				<section id="footer">
					<div class="container">
						<header>
							<h2>Quer um orçamento ou tem dúvidas? <strong>Entre em contato:</strong></h2>
						</header>
						<div class="row">
							<div class="col-6 col-12-medium">
								<section>
									<form role="form" method="post">
										<div class="row gtr-50">
											<div class="col-6 col-12-small">
												<input name="name" placeholder="Nome" type="text" required>
											</div>
											<div class="col-6 col-12-small">
												<input name="email" placeholder="Email" type="text" required>
											</div>
											<div class="col-12">
												<textarea name="message" placeholder="Mensagem" type="text" required></textarea>
											</div>
											<div class="col-12">
												<input type="submit" class="form-button button icon solid fa-envelope" name="enviar" id="enviar" value="Enviar mensagem">
											</div>
										</div>
									</form>
								</section>
							</div>
							<div class="col-6 col-12-medium">
								<section>
									<p>Soluções simples e específicas para resolver os problemas da sua empresa. Atendimento online e presencial. Faça o seu orçamento, de maneira simplificada.</p>
									<div class="row">
										<div class="col-6 col-12-small">
											<ul class="icons">
												<li class="icon solid fab fa-home">
													Rua Jundiaí, 626<br />
													Indaiatuba, SP 13339-310<br />
													BRA
												</li>
												<li class="icon solid fab fa-phone">
													(19)9 8165-2077
												</li>
											</ul>
										</div>
										<div class="col-6 col-12-small">
											<ul class="icons">
												<li class="icon brands fab fa-instagram">
													<a href="https://www.instagram.com/sovaitco">instagram.com/sovaitco</a>
												</li>
												<li class="icon brands fab fa-linkedin">
													<a href="https://www.linkedin.com/company/sovaitco">linkedin.com/company/sovaitco</a>
												</li>
												<li class="icon brands fab fa-facebook-f">
													<a href="https://www.facebook.com/sovaitco">facebook.com/sovaitco</a>
												</li>
											</ul>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
					<div id="copyright" class="container">
						<ul class="links">
							<li>&copy; SOVA IT Co. All rights reserved.</li><li>Design: <a href="http://html5up.net">SOVA IT Co.</a></li>
						</ul>
					</div>
				</section>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>